class MethodsTest {
	public static void main (String []args){
	int x = 5;
	System.out.println(x);
	methodNoInputNoReturn();
	System.out.println(x);
	
	System.out.println(x);
	methodOneInputNoReturn(x+10);
	System.out.println(x);
	
	methodTwoInputNoReturn(4,4.5);
	
	int z = methodNoInputReturnInt();
	System.out.println(z);
	double y = sumSquareRoot(9,5);
	System.out.println(y);
	
	String s1 = "java";
	String s2 = "programming";
	int h = s1.length();
	int g = s2.length();
	System.out.println("The word length is "+h );
	System.out.println("The long word length is "+g );
		
	SecondClass sc = new SecondClass();
	System.out.println(sc.addOne(50));
	System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn(){
	System.out.println("I'm in a method that takes no input and returns nothing.");
	int x = 20;
	System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int yolanda){
	System.out.println("Inside the method one input no return.");
	yolanda = yolanda-5;
	System.out.println(yolanda);
	}
	
	public static void methodTwoInputNoReturn(int hi,double hello){
	System.out.println(hi);	
	System.out.println(hello);
	}
	
	public  static int methodNoInputReturnInt(){
	return 5;	
	}
	
	public static double sumSquareRoot(int what,int why){
	double who = what+why;
	who = Math.sqrt(who);
	return who;
	}
}